import * as React from "react";
import { render } from 'react-dom';
import { Admin, Resource } from 'react-admin';
import DataProvider from './Provider/DataProvider';
import AuthProvider from './Provider/AuthProvider';
import Resources from './Resources';
import { PartnerList, PartnerEdit, PartnerCreate, PartnerIcon } from './Views/res_partner';
const App = () => (
  <Admin dataProvider={DataProvider} authProvider={AuthProvider} >
    <Resource name="res.partner" list={PartnerList} edit={PartnerEdit} create={PartnerCreate} icon={PartnerIcon} />
    <Resource name="product.template" list={PartnerList} edit={PartnerEdit} create={PartnerCreate} icon={PartnerIcon} />
    <Resource name="sale.order" list={PartnerList} edit={PartnerEdit} create={PartnerCreate} icon={PartnerIcon} />
  </Admin>);
;
render(
  <App />,
  document.getElementById('root')
);

  // If you want to start measuring performance in your app, pass a function
  // to log results (for example: reportWebVitals(console.log))
  // or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
  // reportWebVitals();

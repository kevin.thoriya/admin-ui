import * as React from "react";
import { Resource, ListGuesser } from 'react-admin';
import { PartnerList, PartnerEdit, PartnerCreate, PartnerIcon } from '../Views/res_partner';

export default function Resources() {
    return (
        <>
            <Resource name="res.partner" list={PartnerList} edit={PartnerEdit} create={PartnerCreate} icon={PartnerIcon} />
        </>
    )
}

import Config from '../config'
import HTTP from '../Services/http'

export default {
    login: async ({ username, password }) => {
        const data = {
            "jsonrpc": "2.0",
            "params": {
                login: username,
                password: password,
                db: Config.odoo.dbName
            }
        }
        const res = await HTTP.POST('/web/session/authenticate', data);
        if (res && res.error) {
            console.log("ERROR : ", res);
        } else {
            localStorage.setItem('loginUser', JSON.stringify(res));
        }
    },

    checkError: (error) => {
        console.log(error)
    },

    checkAuth: () => {
        localStorage.getItem('loginUser');
        console.log(JSON.parse(localStorage.getItem('loginUser')));
        return localStorage.getItem('loginUser') ? Promise.resolve() : Promise.reject();
    },

    logout: async () => {
        try {
            const res = await fetch("/web/session/logout?redirect=/");
            console.log(res)
            return localStorage.removeItem('loginUser');
        } catch (err) {
            return console.log(err);
        }
    },

    getIdentity: () => { /* ... */ },

    getPermissions: () => {
        const role = localStorage.getItem('permissions');
        return role ? Promise.resolve(role) : Promise.reject();
    }
};
import { fetchUtils } from 'react-admin';
import { stringify } from 'query-string';

const apiUrl = 'http://127.0.0.1:8069';
const httpClient = fetchUtils.fetchJson;

export default {
    getList: async (resource, params) => {
        const { page, perPage } = params.pagination;
        const { field, order } = params.sort;
        //     filter: JSON.stringify(params.filter),
        const url = `/web/dataset/search_read`;
        const requestParams = getBodyFromParams({
            offset: (page - 1) * perPage,
            limit: perPage,
            model: resource,
            sort: `${field} ${order}`,
            domain: [],
            fields: ['id', 'name'], //params.fields ,
        }, false)
        // console.log(ReqParams);
        const headers = new Headers({ 'Access-Control-Allow-Origin': '*', 'Access-Control-Expose-Headers': 'Content-Range', Accept: 'application/json' });
        const { json } = await httpClient(url, {
            method: 'POST',
            headers: headers,
            body: requestParams,
        });
        if (json.result) {
            return {
                data: json.result.records,
                total: json.result.length || 100,
            };
        } else {
            console.log("error ", json);
            return { data: [], total: 0 };
        }
    },

    getOne: async (resource, params) => {
        const { json } = await httpClient(`/web/dataset/call_kw/${resource}/read`, {
            method: 'POST',
            body: getBodyFromParams({
                args: [[params.id], params.fields || []],
                method: "read",
                model: resource,
            }),
        });
        return ({
            data: json,
        });
    },

    getMany: async (resource, params) => {
        const { json } = await httpClient(`/web/dataset/call_kw/${resource}/read`, {
            method: 'POST',
            body: getBodyFromParams({
                args: [[params.ids], params.fields || []],
                method: "read",
                model: resource,
            }),
        });
        return ({
            data: json,
        });
    },

    getManyReference: (resource, params) => {
        const { page, perPage } = params.pagination;
        const { field, order } = params.sort;
        const query = {
            sort: JSON.stringify([field, order]),
            range: JSON.stringify([(page - 1) * perPage, page * perPage - 1]),
            filter: JSON.stringify({
                ...params.filter,
                [params.target]: params.id,
            }),
        };
        const url = `${apiUrl}/${resource}?${stringify(query)}`;

        return httpClient(url).then(({ headers, json }) => ({
            data: json,
            total: parseInt(headers.get('content-range').split('/').pop(), 10),
        }));
    },

    update: async (resource, params) => {
        const { json } = await httpClient(`/web/dataset/call_kw/${resource}/write`, {
            method: 'POST',
            body: getBodyFromParams({
                args: [[parseInt(params.id)], params.data || {}],
                method: "write",
                model: resource,
            })
        });
        return ({
            data: json,
        });
    },


    updateMany: async (resource, params) => {
        const { json } = await httpClient(`/web/dataset/call_kw/${resource}/write`, {
            method: 'POST',
            body: getBodyFromParams({
                args: [[params.ids], params.data || {}],
                method: "write",
                model: resource,
            })

        });
        return ({
            data: json,
        });
    },

    create: async (resource, params) => {
        const { json } = await httpClient(`/web/dataset/call_kw/${resource}/create`, {
            method: 'POST',
            body: getBodyFromParams({
                args: [params.data || {}],
                method: "create",
                model: resource,
            })
        });
        return ({
            data: json,
        });
    },

    delete: async (resource, params) => {
        const { json } = await httpClient(`/web/dataset/call_kw/${resource}/unlink`, {
            method: 'POST',
            body: getBodyFromParams({
                args: [[parseInt(params.id)]],
                method: "unlink",
                model: resource,
            })
        });
        return ({ data: json });
    },

    deleteMany: async (resource, params) => {
        const { json } = await httpClient(`/web/dataset/call_kw/${resource}/unlink`, {
            method: 'POST',
            body: getBodyFromParams({
                args: [[params.ids]],
                method: "unlink",
                model: resource,
            })
        });
        return ({ data: json });
    },
};

export function getUserContext() {
    const userData = localStorage.getItem('loginUser') ? JSON.parse(localStorage.getItem('loginUser')) : {};
    return userData['user_context'];
}

export function getBodyFromParams(data, includeKW = true) {
    return JSON.stringify({
        jsonrpc: "2.0",
        method: "call",
        params: includeKW ? {
            kwargs: { context: getUserContext() },
            ...data
        } : data,
    })
}
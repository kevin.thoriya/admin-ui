import * as React from "react";
import { List, Datagrid, Edit, Create, SimpleForm, DateField, TextField, EditButton, TextInput, DateInput } from 'react-admin';
import BookIcon from '@material-ui/icons/Book';
export const PartnerIcon = BookIcon;
export const PartnerList = (props) => (
    <List {...props}>
        <Datagrid>
            <TextField source="id" />
            <TextField source="name" />
            <TextField source="user_app_version" />
            <TextField source="created_date" />
            {/* <TextField source="average_note" />
            <TextField source="views" /> */}
            <EditButton basePath="/res.partner" />
        </Datagrid>
    </List>
);

const PartnerTitle = ({ record }) => {
    return <span>Partner {record ? `"${record.name}"` : ''}</span>;
};

export const PartnerEdit = (props) => (
    <Edit title={<PartnerTitle />} {...props}>
        <SimpleForm>
            {/* <TextInput disabled source="id" /> */}
            <TextInput source="name" />
            <TextInput source="user_app_version" options={{ multiline: true }} />
            {/* <DateInput label="Created date" source="created_date" /> */}
            {/* <TextInput multiline source="body" />
            <DateInput label="Publication date" source="published_at" />
            <TextInput source="average_note" />
            <TextInput disabled label="Nb views" source="views" /> */}
        </SimpleForm>
    </Edit>
);

export const PartnerCreate = (props) => (
    <Create title="Create a Partner" {...props}>
        <SimpleForm>
            <TextInput source="name" />
            <TextInput source="user_app_version" options={{ multiline: true }} />
        </SimpleForm>
    </Create>
);
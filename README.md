### set config 
- package.json : set the proxy to point on server
- src/config/index.js : config the database

### install the packages
``` npm install ```

### run the application on port 3000
``` npm start ```